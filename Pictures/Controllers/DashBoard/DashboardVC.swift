//
//  DashboardVC.swift
//  Pictures
//
//  Created by Admin on 15/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class DashboardVC: UIViewController {

    @IBOutlet var viewActivity: UIActivityIndicatorView!
    @IBOutlet var btnRecent: UIButton!
    @IBOutlet var btnInterest: UIButton!
    @IBOutlet var listStack: UIStackView!
    @IBOutlet var collectionPictures: UICollectionView!
    var listPictures = [AnyObject]()
    var pageTo = 10
    var pageFrom = 0
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpData()
        
    }
    
    // MARK: - Load All data
    
    func setUpData()
    {
        self.btnInterest.setTitleColor(UIColor.white, for: .normal)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.ThemeColor]
        self.collectionPictures.register(UINib(nibName: "PictureCVCell", bundle: nil), forCellWithReuseIdentifier: "PictureCVCell")
        
        // Default api call to get Most interesting data
        getMostInterestingData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Api call for Most Interesting Data
    
    func getMostInterestingData()
    {
        if isLoading == true
        {
            self.pageTo += 10
            self.pageFrom += 10
            self.viewActivity.isHidden = false
        }
        WebServices().callJSONWebApi(.listOfPhotos, pagination: isLoading, fromPage: self.pageFrom, toPage: self.pageTo, withHTTPMethod: .get, forPostParameters: nil) { (serviceResponse) in
            print(serviceResponse)
            if (serviceResponse)["query"] as? [String:Any] != nil
            {
                if ((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject]).count != 0
                {
                    for i in 0...((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject]).count-1
                    {
                        self.listPictures.append((((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject])[i]))
                    }
                    self.viewActivity.isHidden = true
                    self.collectionPictures.reloadData()
                }
            }
        }
    }
    
    // MARK: - Api call for Most Recent Data
    
    func getMostRecentData()
    {
        // Check for pagination
        if isLoading == true
        {
            self.pageTo += 10
            self.pageFrom += 10
            self.viewActivity.isHidden = false
        }
        WebServices().callJSONWebApi(.detailPicture, pagination: isLoading, fromPage: self.pageFrom, toPage: self.pageTo, withHTTPMethod: .get, forPostParameters: nil) { (serviceResponse) in
            print(serviceResponse)
            if (serviceResponse)["query"] as? [String:Any] != nil
            {
                if ((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject]).count != 0
                {
                    for i in 0...((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject]).count-1
                    {
                        self.listPictures.append((((((serviceResponse)["query"] as! [String:Any])["results"] as! [String:Any])["photo"] as! [AnyObject])[i]))
                    }
                    self.viewActivity.isHidden = true
                    self.collectionPictures.reloadData()
                }
            }
        }
    }
    
    // MARK: - Action to switch tabs
    
    @IBAction func btn_MostInterestingAction(_ sender: UIButton)
    {
        self.pageTo = 10
        self.pageFrom = 0
        self.listPictures.removeAll()
        self.isLoading = false
        self.btnInterest.setTitleColor(UIColor.ThemeColor, for: .normal)
        self.btnRecent.setTitleColor(UIColor.ThemeColor, for: .normal)
        self.btnInterest.backgroundColor = UIColor.white
         self.btnRecent.backgroundColor = UIColor.white
        
        if sender.backgroundColor == UIColor.ThemeColor
        {
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.ThemeColor, for: .normal)
        }
        else
        {
            sender.backgroundColor = UIColor.ThemeColor
            sender.setTitleColor(UIColor.white, for: .normal)
        }
        
        if sender.tag == 1
        {
           getMostInterestingData()
        }
        else
        {
            getMostRecentData()
        }
    }
    
}

// MARK: - Display api data

extension DashboardVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.listPictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCVCell", for: indexPath) as! PictureCVCell
            
            let url = URL(string: "http://farm\(self.listPictures[indexPath.row]["farm"] as! String).staticflickr.com/\(self.listPictures[indexPath.row]["server"] as! String)/\(self.listPictures[indexPath.row]["id"] as! String)_\(self.listPictures[indexPath.row]["secret"] as! String)_t_d.jpg")
            cell.imgPicture.kf.setImage(with: url!, placeholder: #imageLiteral(resourceName: "loader"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
            cell.lblTitle.text = self.listPictures[indexPath.row]["title"] as? String
        
            if indexPath.row == self.listPictures.count - 1 { // last cell
              loadMoreItems(indexpath: indexPath.row)
           }
           
            return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // MARK: - Navigate to display original picture
        
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "DetailPageVC") as! DetailPageVC
         let url = URL(string: "http://farm\(self.listPictures[indexPath.row]["farm"] as! String).staticflickr.com/\(self.listPictures[indexPath.row]["server"] as! String)/\(self.listPictures[indexPath.row]["id"] as! String)_\(self.listPictures[indexPath.row]["secret"] as! String).jpg")
          destination.imgUrl = url
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.pushViewController(destination, animated: false)
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         return CGSize(width: collectionView.frame.size.width/2, height: collectionView.frame.size.height/3)
    }
}

 // MARK: - To get more data from api

extension DashboardVC
{
    func loadMoreItems(indexpath:Int)
    {
       if self.btnInterest.currentTitleColor == UIColor.white
       {
           self.isLoading = true
           getMostInterestingData()
       }
        else
       {
          self.isLoading = true
          getMostRecentData()
       }
    }
}
