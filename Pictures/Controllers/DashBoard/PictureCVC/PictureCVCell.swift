//
//  PictureCVCell.swift
//  Pictures
//
//  Created by Admin on 16/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class PictureCVCell: UICollectionViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgPicture: UIImageView!
    @IBOutlet var viewBorder: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
