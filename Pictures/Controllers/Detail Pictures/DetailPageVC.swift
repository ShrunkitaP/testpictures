//
//  DetailPageVC.swift
//  Pictures
//
//  Created by Admin on 16/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class DetailPageVC: UIViewController {

    @IBOutlet var imgPicture: UIImageView!
    var imgUrl : URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // MARK: - Display original image
        
         self.imgPicture.kf.setImage(with: imgUrl!, placeholder: #imageLiteral(resourceName: "loader"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_BackAction(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
