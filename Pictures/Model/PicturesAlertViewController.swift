//
//  PicturesAlertViewController.swift
//  Pictures
//
//  Created by Admin on 16/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class PicturesAlertViewController: UIAlertController {
    
    static let shared = PicturesAlertViewController(title: "Pictures", message: nil, preferredStyle: .alert)
    var actionAfterDismiss: (()->Void)!
    var appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - Alert Ok Action
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            guard self.actionAfterDismiss != nil else {
                return
            }
            self.actionAfterDismiss()
        }
        addAction(okAction)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Present Alert Message
    
    func presentAlertController(title: String = "Pictures", message: String, completionHandler: (()->Void)?) {
        self.title = title
        self.message = message
        actionAfterDismiss = completionHandler
        UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
