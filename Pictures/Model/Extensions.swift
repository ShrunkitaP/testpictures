//
//  Extensions.swift
//  Axess
//
//  Created by HPL on 18/09/18.
//  Copyright © 2018 HPL. All rights reserved.
//

import UIKit

extension UIColor {
    class var ThemeColor: UIColor
    {
        get
        {
            return UIColor(red: 5/255, green: 64/255, blue: 120/255, alpha: 1) //Blue color
        }
    }

}
