//
//  WebService.swift
//  Pictures
//
//  Created by Admin on 16/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SystemConfiguration
import MBProgressHUD


enum UserServices : String {
    case listOfPhotos  = "flickr.photos.interestingness"
    case detailPicture = "flickr.photos.recent"
}

enum HTTPMethod : String {
    case post = "POST"
    case get = "GET"
}


typealias actionWithServiceResponse = ((_ serviceResponse: [String:Any])-> Void)

class WebServices {
    
    // MARK: - Static property to access Singleton Class
    static let shared = WebServices()
    var window: UIWindow?
    
    // MARK: - Base url for web api
    private let baseurl = "https://query.yahooapis.com/v1/public/"//live
    
     // MARK: - Api key for web api
    private let apiKey = "d5c7df3552b89d13fe311eb42715b510"
    
    //GCD initialize Global User Interactive Queue
    private let userInteractiveGlobalQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
    
    //Instance of URLSession to interact with WebAPI(s)
    private let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    
    //activity indicator while waiting for Web Api response and to stop user interaction
    fileprivate var progressHUD: MBProgressHUD!
    
    
    // MARK: - Perform to get all parameters use for api
    
    public func callJSONWebApi(_ api: UserServices,pagination : Bool,fromPage : Int,toPage : Int,withHTTPMethod method: HTTPMethod, forPostParameters parameters: [String:Any]!,actionAfterServiceResponse completionHandler: @escaping actionWithServiceResponse)
    {
        var request: URLRequest!
        if pagination != true
        {
            self.showHUD()
        }
        if method == .post
        {
            request = URLRequest(url: URL(string: "\(self.baseurl)yql?q=select%20*%20from%20\(api.rawValue)%28\(fromPage)%2C\(toPage)%29%20where%20api_key%3D%27\(apiKey)%27&diagnostics=true&format=json")!)
            if parameters != nil
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            }
        }
        else
        {
        
            request = URLRequest(url: URL(string: "\(self.baseurl)yql?q=select%20*%20from%20\(api.rawValue)%28\(fromPage)%2C\(toPage)%29%20where%20api_key%3D%27\(apiKey)%27&diagnostics=true&format=json")!)
        }
        
        print(request.url!)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        guard checkForNetworkConnectivity() else
        {
            return
        }
        
  
        userInteractiveGlobalQueue.async
        {
           self.callWebServiceWithRequest(request ,actionAfterServiceResponse: completionHandler)
        }
    }
    
    
    // MARK: -perform dataTask(s) for Web Service(s)
    
    private func callWebServiceWithRequest(_ request: URLRequest, actionAfterServiceResponse completionHandler: @escaping actionWithServiceResponse) {
        let dataTask = defaultSession.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                self.progressHUD?.hide(animated: true)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard error == nil else {
                    PicturesAlertViewController.shared.presentAlertController(message: (error?.localizedDescription)!, completionHandler: nil)
                    return
                }
                do {
                    let responseData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as! [String:Any]
                    print(responseData)
                    
                    if let _ = data,
                        let response = response as? HTTPURLResponse,
                        response.statusCode == 200
                    {
                       completionHandler(responseData)
                    }
                    else
                    {
                        PicturesAlertViewController.shared.presentAlertController(message: error as! String, completionHandler: nil)
                    }
                    
                }
                catch
                {
                    PicturesAlertViewController.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        dataTask.resume()
    }
  
    
}

extension WebServices {
    
    // MARK: - possible states for internet access
    
    private enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    private var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
    
    
    // MARK: - check for internet access
    
    fileprivate func checkForNetworkConnectivity() -> Bool {
        guard self.currentReachabilityStatus != .notReachable else {
            
            PicturesAlertViewController.shared.presentAlertController(title: "Connection Problem", message: "Please check your internet connection !", completionHandler: nil)
            return false
        }
        return true
    }
    
     // MARK: - Display Progress Loader
    
    fileprivate func showHUD() {
        progressHUD = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
    }
}



